//
//  DeviceOrientationViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by Cheney on 2023/3/13.
//

import UIKit

class DeviceOrientationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
    }
    
    @IBAction func ActionClick(_ sender: UISwitch) {
        SensorsAnalyticsSDK.sharedInstance()?.enableTrackScreenOrientation(sender.isOn)
    }
    

}
