//
//  FirstInstallViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by Cheney on 2023/3/13.
//

import UIKit

class FirstInstallViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        SensorsAnalyticsSDK.sharedInstance()?.trackAppInstall()
    }
    

}
