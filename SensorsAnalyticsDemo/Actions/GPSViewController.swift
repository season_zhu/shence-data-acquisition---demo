//
//  GPSViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by Cheney on 2023/3/13.
//

import UIKit

class GPSViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func LocationClick(_ sender: UISwitch) {
        SensorsAnalyticsSDK.sharedInstance()?.enableTrackGPSLocation(sender.isOn)
    }

    
}
