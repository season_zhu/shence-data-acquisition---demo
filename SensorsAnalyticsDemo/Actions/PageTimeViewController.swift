//
//  PageTimeViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by Cheney on 2023/3/13.
//

import UIKit

class PageTimeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SensorsAnalyticsSDK.sharedInstance()?.trackTimerStart("PageTimeViewController")
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SensorsAnalyticsSDK.sharedInstance()?.trackTimerEnd("PageTimeViewController")

    }

}
