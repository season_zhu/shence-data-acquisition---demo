//
//  ConnectUserViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by Cheney on 2023/3/13.
//

import UIKit

class ConnectUserViewController: UIViewController {

    @IBOutlet weak var userIdTF: UITextField!
    
    @IBOutlet weak var loginKeyTF: UITextField!
    
    private lazy var tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(tap)
    }
    
    @objc
    private func tapAction() {
        userIdTF.resignFirstResponder()
        loginKeyTF.resignFirstResponder()
    }
    
    
    //简易关联
    @IBAction func sampleClick(_ sender: Any) {
        SensorsAnalyticsSDK.sharedInstance()?.login(userIdTF.text ?? "userID")
    }
    
    //全域关联
    @IBAction func allClick(_ sender: Any) {
        SensorsAnalyticsSDK.sharedInstance()?.login(withKey: loginKeyTF.text ?? "loginKey", loginId: userIdTF.text ?? "userID")

    }
    
}
