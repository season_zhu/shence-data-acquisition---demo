//
//  AllPointViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by Cheney on 2023/3/13.
//

import UIKit

class AllPointViewController: UIViewController {

    @IBOutlet weak var ImageView: UIImageView!
    
    @IBOutlet weak var TextField: UITextField!
    @IBOutlet weak var TextView: UITextView!
    
    @IBOutlet weak var ImageLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action:#selector(ImageViewClick))
        ImageView.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action:#selector(ImageLabelClick))
        ImageLabel.addGestureRecognizer(tap1)
        
    }
    
    @objc func ImageViewClick() {
        
    }
    
    @objc func ImageLabelClick() {
        
    }

    @IBAction func ActionBtnClick(_ sender: UIButton) {
        
    }
    
    @IBAction func NextClick(_ sender: Any) {
    }
    
    
    @IBAction func Switch(_ sender: Any) {
    }
    
    @IBAction func segementClick(_ sender: Any) {
    }
    
    @IBAction func StepClick(_ sender: Any) {
    }
    
    @IBAction func progroessViewSlider(_ sender: Any) {
    }
    
    @IBAction func PageControllClick(_ sender: Any) {
    }
    
}

