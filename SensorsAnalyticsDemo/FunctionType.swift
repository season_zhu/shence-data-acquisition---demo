//
//  FunctionType.swift
//  SensorsAnalyticsDemo
//
//  Created by dy on 2023/3/13.
//

import Foundation

/**
 1.全埋点 （$AppStart (App 启动）、$AppEnd （App 退出）、$AppViewscreen（浏览页面） 和 $AppClick（控件点击））

 2.App安装

 3.关联登录用户

 4.页面时长

 5.屏募方向

 6.gps

 7.crash采集

 8.主动事件上报
 */

enum FunctionType: CaseIterable {
    case fullAutoTrack
    
    case firstLaunch
    
    case associateUser
    
    case pageTime
    
    case screenOrientations
    
    case gps
    
    case crash
    
    case activeReport
}

extension FunctionType {
    var text: String {
        switch self {
        case .fullAutoTrack:
            return "全埋点"
        case .firstLaunch:
            return "第一次安装进入App"
        case .associateUser:
            return "关联用户"
        case .pageTime:
            return "页面时长"
        case .screenOrientations:
            return "屏募方向"
        case .gps:
            return "GPS"
        case .crash:
            return "crash采集"
        case .activeReport:
            return "主动事件上报"
        }
    }
    
    var controller: UIViewController {
        switch self {
        case .fullAutoTrack:
            return AllPointViewController()
        case .firstLaunch:
            return FirstInstallViewController()
        case .associateUser:
            return ConnectUserViewController()
        case .pageTime:
            return PageTimeViewController()
        case .screenOrientations:
            return DeviceOrientationViewController()
        case .gps:
            return GPSViewController()
        case .crash:
            return CrashController()
        case .activeReport:
            return EventUploadViewController()
        }
    }
}
