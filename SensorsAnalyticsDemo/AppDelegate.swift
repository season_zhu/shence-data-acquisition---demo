//
//  AppDelegate.swift
//  SensorsAnalyticsDemo
//
//  Created by dy on 2023/3/13.
//

import UIKit

let Sa_Default_ServerURL = "https://pre-api-gateway.cu-sc.com:19098/open-api/behavior-collection-bs-demo/api/behavior/acquisition" //"http://sdk-test.cloud.sensorsdata.cn:8006/sa?project=default&token=95c73ae661f85aa0"

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //初始化
        let config = SAConfigOptions(serverURL: Sa_Default_ServerURL, launchOptions: launchOptions)
        config.autoTrackEventType = [.eventTypeAppStart, .eventTypeAppEnd, .eventTypeAppClick, .eventTypeAppViewScreen]
        config.enableVisualizedAutoTrack = true
        config.enableTrackPageLeave = true
        config.enableTrackChildPageLeave = true
        config.enableHeatMap = true
        config.enableLog = true
        config.enableJavaScriptBridge = true
        config.maxCacheSize = 20000
        config.flushInterval = 10 * 1000
        config.flushNetworkPolicy = .typeALL
        config.enableTrackAppCrash = true
        config.instantEvents
        SensorsAnalyticsSDK.start(configOptions: config)
        return true
    }


}

