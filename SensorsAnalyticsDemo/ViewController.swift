//
//  ViewController.swift
//  SensorsAnalyticsDemo
//
//  Created by dy on 2023/3/13.
//

import UIKit

import SnapKit

class ViewController: UIViewController {

    private lazy var tableView = UITableView(frame: .zero, style: .plain)
    
    let dataSource = FunctionType.allCases
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        
        title = "埋点功能"
        
        /// 注册cell
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.className)
        
        /// 设置tableFooterView
        tableView.tableFooterView = UIView()
        
        /// 设置数据源与代理
        tableView.dataSource = self
        tableView.delegate = self
        
        /// 简单布局
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataSource[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.className)!
        cell.textLabel?.text = item.text
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let item = dataSource[indexPath.row]
        navigationController?.pushViewController(item.controller, animated: true)
    }
}
